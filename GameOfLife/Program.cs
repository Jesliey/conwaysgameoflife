﻿///Game Of Life
///Rules:   1 - A Cell is either alive or Dead
///         2 - A Living Cell with less than 2 neighbors dies
///         3 - A Living Cell with 2 or 3 neighbors lives
///         4 - A Living Cell with more than 3 neighbors dies
///         5 - A Dead Cell with exactly 3 neighbors becomes alive
using System;

namespace GameOfLife
{
    class Program
    {
        static bool[,] board;

        static void Main(string[] args)
        {

            string input = "";
            while(input.ToLower() != "exit")
            {
                Console.Clear();
                Console.WriteLine("Welcome to the Game of Life!");
                Console.Write("Please enter a board size, or type 'Exit' to exit: ");
                input = Console.ReadLine();
                if(input!="Exit") input = GameStart(input);
            }
        }

        private static string GameStart(string input)
        {
            //man why is core gameplay loop so fucking tedious in consoles???
            Console.Clear();

            int boardSize = 0;
            int spawnPercentage = 0;
            int gameStages = 0;

            //first grab board size
            try{
                boardSize = int.Parse(input);
                //Console.WriteLine("Creating Game board...");
                if (boardSize < 0) throw new Exception();
            }
            catch (Exception){
                Console.WriteLine("Enter a valid Board Size or 'Exit'. ");
                Console.Write("Press Enter to continue...");
                Console.ReadLine();
                return "error lmao";
            }

            //then grab cell spawn chance
            Console.Write("Please enter a percentage chance that a cell starts alive: ");
            try
            {
                spawnPercentage = int.Parse(Console.ReadLine());
                if (spawnPercentage < 0) throw new Exception();
            }
            catch (Exception)
            {
                Console.WriteLine("Invalid Spawn Chance entered. Returning to Main.");
                Console.Write("Press Enter to continue...");
                Console.ReadLine();
                return "error lmao";
            }

            Console.WriteLine("Looks good to me. Creating Gameboard.");
            CreateBoard(boardSize, spawnPercentage);

            ///
            ///THIS IS WHERE I LEFT OFF
            ///TODO:
            ///     Create play options. Either step by step or run through the whole stepcount.
            ///     Test that it works :/
            ///

            Console.Write("How many Game Stages do you want the board to advance?: ");
            try
            {
                gameStages = int.Parse(Console.ReadLine());
                if (gameStages < 0) throw new Exception();

            }
            catch (Exception e)
            {
                Console.WriteLine("Invalid number of Game Stages. Returning to Main.");
                Console.Write("Press Enter to continue...");
                Console.ReadLine();
                return "error lmao";
            }

            string RunOrWalk = "";
            while (RunOrWalk.ToLower() != "y" && RunOrWalk.ToLower() != "n")
            {
                Console.Clear();
                Console.Write("Do you want Life to run continuously for all Game Stages? (Y/N): ");
                RunOrWalk = Console.ReadLine();
            }
            //Console.ReadLine();//please dont exit immediately i need to test TwT
            PlayLife(1, gameStages, RunOrWalk);
            return "";
        }

        private static void CreateBoard(int boardSize, int spawnPercentage)
        {
            Console.WriteLine("Creating game board...");
            board =  new bool[boardSize, boardSize];//create new bool array by passed square size
            Random r = new Random();//random ints to decide starting state
            int rand;
            Console.WriteLine("Randomizing live cells...");
            for (int i = 0; i < board.GetLength(0); i++)
            {
                for (int j = 0; j < board.GetLength(1); j++)
                {
                    rand = r.Next(0, 100);//percent chance time
                    if (rand < spawnPercentage) board[i, j] = true;
                    else board[i, j] = false;
                }
            }
            //Console.Write("Success. Press any key to continue...");
            //Console.Read();
            //PrintBoard();
        }

        private static void PrintBoard(int StepBit) {
            Console.Clear();
            string boardprint = "";
            for (int i = 0; i < board.GetLength(0); i++)
            {
                string line = "";
                for (int j = 0; j < board.GetLength(1); j++)
                {
                    if(board[i,j]) line += "[" + 1 + "] ";//man i really should have used an int array instead of bools :/
                    else line += "[" + 0 + "] ";
                }
                boardprint = boardprint + line + "\n";
            }
            Console.WriteLine(boardprint);
            if (StepBit == 0) Console.ReadLine();
        }

        private static void PlayLife(int currentStep, int maxStep, string RunOrWalk)
        {
            if (RunOrWalk == "n" || RunOrWalk == "no") PrintBoard(0);
            else PrintBoard(1);
            
            int neighbors;
            bool[,] newBoard = new bool[board.GetLength(0), board.GetLength(1)];

            for (int i = 0; i < board.GetLength(0); i++)
            {
                for (int j = 0; j < board.GetLength(1); j++)
                {
                    neighbors = CheckNeighbors(i, j);

                    if(board[i,j] == true)//are you alive?
                    {
                        switch (neighbors)
                        {
                            case 0:
                            case 1:
                                newBoard[i, j] = false;//not for long youre not
                                break;
                            case 2:
                            case 3:
                                newBoard[i, j] = true;//yes
                                break;
                            default:
                                newBoard[i, j] = false;//you starve to death
                                break;
                        }
                    }
                    else//will you be alive tomorrow?
                    {
                        if (neighbors == 3) newBoard[i, j] = true;//survey says yes
                        else newBoard[i, j] = false;
                    }
                }
            }
            board = newBoard;//the dawn of a new day

            if (currentStep != maxStep) PlayLife(currentStep + 1, maxStep, RunOrWalk);
            else
            {
                Console.WriteLine("Game Over! That's Life, folks!");
                Console.Read();
            }
        }

        private static int CheckNeighbors(int x, int y)
        {
            int count = 0;
            for (int i = -1; i < 2; i++)//cell x neighbors
            {
                if (x+i>=0 && x+i<board.GetLength(0))//stay within grid bounds
                {
                    for (int j = -1; j < 2; j++)//cell y neighbors
                    {
                        if (y+j>=0 && y+j<board.GetLength(1))//stay within grid bounds
                        {
                            //if ((i != 0 && j != 0) && board[x + i, y + j] == true) count++;
                            if (i == 0 && j == 0) continue;
                            else
                            {
                                if (board[x + i, y + j] == true) count++;
                            }
                        }
                    }
                }
            }

            return count;
        }
    }
}
